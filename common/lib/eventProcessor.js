
'use strict'
 var QueueProcessor = require('../lib/processors/QueueProcessor');

var queue="https://sqs.ap-south-1.amazonaws.com/939371275774/SOMETHING_dev";
class GenericConsumer{


    constructor()
    {
      this.counter=0;
      this.ancounter=0;
      this.avgdiff=0;
      this.high=0;
      this.low=9999999999;
      this.env= process.env.NODE_ENV || 'development';
      this._messageHandler = this.messageHandler.bind(this);
      this.qProcessor= new QueueProcessor(this._messageHandler,{visibililtytimeout:30});

      this.qProcessor.setQueueURL(queue);

    }
    start(){
      this.qProcessor.start();
    }
    stop(){
      this.qProcessor.stop();
    }
    
    messageHandler (message, done){
        
        this.counter++;
        this.ancounter++
        var now = new Date().getTime();
        var then = new Date(message.ts).getTime();
        var diff=(now-then)/1000;
        this.avgdiff+= diff;
        if (this.high <= diff)this.high=diff;
        if(this.low >=diff)this.low =diff;
        if(this.ancounter == 100 )
        {
            console.log (" -----------------------------------------------------------------------------------------")
            console.log ( "Processed a 100 messages at avg Time delta : " + this.avgdiff / this.ancounter  + " sec");
            console.log ("                                     Highest : " + this.high + " sec");
            console.log ("                                     Lowest  : " + this.low + " sec");
            this.ancounter=this.high=0;
            this.avgdiff=0;
            this.low=99999999;
        }
        done();
        
      //DO WHAT EVER REQUIRED
      //If processing fails, throw up !!
      /*
        message
        {
          "ts":"2017-06-17T14:18:22.641Z",
          "user_id":"Some User",
          "et":"UserRegistered",
          "data":{},
          "ed":"USER"
        }
      */

        

    
    
    
    
    }

}


var co = new GenericConsumer();
co.start();


//var co1 = new GenericConsumer();
//co1.start();
//

//var co2 = new GenericConsumer();
//co2.start();
