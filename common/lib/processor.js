
'use strict'
 var QueueProcessor = require('../lib/processors/QueueProcessor');

var queue="https://sqs.ap-south-1.amazonaws.com/939371275774/TODO_local";

class TodoProcessor{

	constructor()
    {
      this.counter=0;
      this.ancounter=0;
      this.avgdiff=0;
      this.high=0;
      this.low=9999999999;
      this.env= process.env.NODE_ENV || 'development';
      this._messageHandler = this.messageHandler.bind(this);
      this.qProcessor= new QueueProcessor(this._messageHandler,{visibililtytimeout:30});

      this.qProcessor.setQueueURL(queue);

    }
    start(){
      this.qProcessor.start();
    }
    stop(){
      this.qProcessor.stop();
    }

    messageHandler( message, done){
    	console.log(" Message recieved ...."+message);
    	done();
    }
}