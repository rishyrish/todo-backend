'use strict';
var TodoEvents=require('baybridge').TodoEvents;
var mBus=require('baybridge').mBus;


module.exports = function(User) {

	User.observe('after save', function(ctx, next) {
	  if (ctx.isNewInstance) {
	    raiseEvent(ctx.instance);
	  }
	  next();
	});


	var raiseEvent = (user) => {
		console.log(" new user created : "+ JSON.stringify(user));
		var ur = new TodoEvents.UserCreated();
		ur.data= {
			userId : user.userid,
			Name : user.name,
			Phone : user.userid,
		}
		mBus.publishEvent(ur);
	}
};
