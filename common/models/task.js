'use strict';
var TodoEvents=require('baybridge').TodoEvents;
var mBus=require('baybridge').mBus;

module.exports = function(Task) {

	Task.observe('after save', function(ctx, next) {
	  if (ctx.isNewInstance) {
	    raiseEvent(ctx.instance);
	  }
	  else {
	  	console.log(" something ");
	  	completeEvent(ctx);
	  }
	  next();
	});

	var raiseEvent = (task) => {
		console.log(" new task created : "+ JSON.stringify(task));
		var tr = new TodoEvents.TaskCreated();
		tr.data= {
			taskId : task.id,
			Name : task.taskName,
			User : task.userid,
		}
		mBus.publishEvent(tr);
	};

	var completeEvent = (task) => {
		console.log(" task completed: "+ JSON.stringify(task));
		var tr = new TodoEvents.TaskCompleted();
		Task.findById(task.where.id)
		.then(res => {
			console.log("here"+res);
			tr.data= {
				taskId : res.id.toString(),
				Name : res.taskName,
				User : res.userid,
			};
		});
		
		mBus.publishEvent(tr);
	};

};
